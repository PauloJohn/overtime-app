require 'rails_helper'

describe 'navigate' do
  describe 'index' do
    before do 
      @user = User.create(email: "test@test.com", password: "asdfasdf", password_confirmation: "asdfasdf", first_name: "jon", last_name: "snow")
      login_as(@user, :scope => :user)
    end
    it 'can be reached successfully' do
      visit posts_path
      expect(page.status_code).to eq(200)
    end

    it 'has a title of posts' do
      visit posts_path
      expect(page).to have_content("Posts")
    end

    it 'has a list of posts' do
      post1 = Post.create(date: Date.today, rationale: "Post1", user_id: @user.id)
      post1 = Post.create(date: Date.today, rationale: "Post2", user_id: @user.id)
      visit posts_path
      expect(page).to have_content(/Post1|Post2/)
    end
  end

  describe 'creation' do
    before do
      user = User.create(email: "test@test.com", password: "asdfasdf", password_confirmation: "asdfasdf", first_name: "jon", last_name: "snow")
      login_as(user, :scope => :user)

      visit new_post_path
    end
    it 'can be created from a new form page' do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[rationale]', with: "Some rationale"
      click_on "Salvar"

      expect(page).to have_content("Some rationale")
    end 
    it 'will have a user associated it' do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[rationale]', with: "User association"
      click_on "Salvar"
      
      expect(User.last.posts.last.rationale).to eq("User association")
    end
  end
end
